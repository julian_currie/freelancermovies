package com.codingwithcurry.freelancerandroidtest.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.OkHttpClient;

public class MovieClient {

    private static Retrofit retrofitClient;

    public static Retrofit getClient() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new MovieNetworkInterceptor()) //This is used to add NetworkInterceptor.
                .build();

        //retrieve the current client if it has already been created
        if(retrofitClient == null) {
            retrofitClient =  new Retrofit.Builder()
                    .baseUrl("https://api.themoviedb.org/3/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }

        return retrofitClient;
    }
}

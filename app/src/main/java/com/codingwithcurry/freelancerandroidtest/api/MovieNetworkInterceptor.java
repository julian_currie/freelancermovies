package com.codingwithcurry.freelancerandroidtest.api;

import com.codingwithcurry.freelancerandroidtest.BuildConfig;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

//I looked up how to add the api key to the url in OkHttp.
//It's been awhile since I've worked with OkHttp
//https://androidclarified.com/okhttp-interceptor-retrofit2-example/
public class MovieNetworkInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {

        //Modify the current request to add the API Key
        //Normally, the apikey.properties wouldn't be committed
        HttpUrl url = chain.request()
                .url()
                .newBuilder()
                .addQueryParameter("api_key", BuildConfig.API_KEY)
                .build();

        Request request = chain.request()
                .newBuilder()
                .url(url)
                .build();

        return chain.proceed(request);
    }
}

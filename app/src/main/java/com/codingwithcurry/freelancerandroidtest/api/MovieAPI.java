package com.codingwithcurry.freelancerandroidtest.api;
import com.codingwithcurry.freelancerandroidtest.model.Results;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MovieAPI {

    //I would normally use
    @GET("movie/popular")
    Call<Results> getMovies();
}

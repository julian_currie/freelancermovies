package com.codingwithcurry.freelancerandroidtest.ui.main;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.codingwithcurry.freelancerandroidtest.api.MovieAPI;
import com.codingwithcurry.freelancerandroidtest.api.MovieClient;
import com.codingwithcurry.freelancerandroidtest.model.Movie;
import com.codingwithcurry.freelancerandroidtest.model.Results;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//helps prevents excessive calls on configuration changes.
public class MovieViewModel extends ViewModel {

    //Using live data. RxJava would be overkill for this project as it is now.
    private MutableLiveData<ArrayList<Movie>> movieLiveData;

    public MovieViewModel() {
        movieLiveData =  new MutableLiveData<>();
        MovieAPI apiInterface = MovieClient.getClient().create(MovieAPI.class);
        apiInterface.getMovies().enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {
                //Update the live data after out REST call has completed
                if(response.body() != null){
                    movieLiveData.setValue(response.body().movieList);
                }
                //else Show an appropriate msg to the user with an Alert
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                //An error callback could go here or you could reset the live data
                Log.d("MovieViewModel", "Error: " + t.getMessage());
            }
        });
    }

    public LiveData<ArrayList<Movie>> getMovieList() {
        return movieLiveData;
    }
}


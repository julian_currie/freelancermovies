package com.codingwithcurry.freelancerandroidtest.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

//This class is needed to account for the structure of the returned JSON
public class Results {

    @SerializedName("results")
    public ArrayList<Movie> movieList;
}

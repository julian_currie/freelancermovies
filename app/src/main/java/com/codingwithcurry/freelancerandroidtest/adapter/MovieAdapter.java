package com.codingwithcurry.freelancerandroidtest.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.codingwithcurry.freelancerandroidtest.R;
import com.codingwithcurry.freelancerandroidtest.model.Movie;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private ArrayList<Movie> movieList;

    public MovieAdapter(ArrayList<Movie> movieList) {
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from (parent.getContext ())
                .inflate (R.layout.movie_card, parent, false);

        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        final Movie movie = movieList.get(position);
        holder.movieTitleTextView.setText(movie.title);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)
                holder.posterImageView.getLayoutParams();

        //The margins are set programmatically depending on what column
        //the image belongs to. This ensures even spacing in the middle only
        if(position % 2 == 0){
            params.setMargins(0, 0, 20, 0);
        } else {
            params.setMargins(20, 0, 0, 0);
        }
        holder.posterImageView.setLayoutParams(params);

        //Lazily load the poster_path field into the ImageView
        Glide.with(holder.posterImageView.getContext())
                .load("https://image.tmdb.org/t/p/w300" + movie.posterPath)
                .transform(new CenterCrop(), new RoundedCornersTransformation(80, 0))
                .into(holder.posterImageView);
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {
        TextView movieTitleTextView;
        ImageView posterImageView;

        MovieViewHolder(final View movieLayout) {
            super(movieLayout);
            movieTitleTextView = movieLayout.findViewById(R.id.title_text);
            posterImageView = movieLayout.findViewById(R.id.poster_image);
        }
    }
}

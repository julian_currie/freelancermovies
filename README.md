# Freelancer movies

An app that displays a list of popular movies from the [Movies DB](www.themoviedb.org) website.

## Getting Started

1. Clone the repo: git clone https://julian_currie@bitbucket.org/julian_currie/freelancermovies.git
2. Sync the gradle files in Android Studio and run

## Features to add
* Add a movie detail screen
* Add movie ratings in the list

## Authors

* **Julian Currie**
